#[inline]
/// Work around transmute not knowing sizes of type parameters in trait impls.
pub unsafe fn cast_val<A, B>(val: A) -> B {
    let out = std::mem::transmute_copy(&val);
    std::mem::forget(val);
    out
}

#[inline]
pub unsafe fn cast_ref<A, B>(val: &A) -> &B {
    &*(val as *const A as *const B)
}

#[inline]
pub unsafe fn cast_mut<A, B>(val: &mut A) -> &mut B {
    &mut *(val as *mut A as *mut B)
}

#[inline]
pub unsafe fn cast_vec<A, B>(mut val: Vec<A>) -> Vec<B> {
    let out = Vec::from_raw_parts(val.as_mut_ptr() as *mut B, val.len(), val.capacity());
    std::mem::forget(val);
    out
}

#[inline]
pub unsafe fn cast_slice<A, B>(val: &[A]) -> &[B] {
    std::slice::from_raw_parts(val.as_ptr() as *const B, val.len())
}

#[inline]
pub unsafe fn cast_slice_mut<A, B>(val: &mut [A]) -> &mut [B] {
    std::slice::from_raw_parts_mut(val.as_mut_ptr() as *mut B, val.len())
}
