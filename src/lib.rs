//! The purpose of convute is to let you convert between types of the same size
//! with overlapping valid bit patterns. To do so, you must implement the unsafe
//! `convute::marker::Transmute` or `convute::marker::TryTransmute` traits on
//! your types. The great thing about these markers is that we can derive
//! conversions between arrays, vecs and slices of those types.
//!
//! ```
//! // Import the traits which implement the conversions.
//! use convute::convert::*;
//!
//! // This type represents a value in the range 0..9.
//! struct Lt9(u8);
//!
//! impl Lt9 {
//!     #[inline]
//!     fn new(val: u8) -> Option<Self> {
//!         if val < 9 { Some(Lt9(val)) } else { None }
//!     }
//! }
//!
//! // Let convute know every Lt9 is a valid u8.
//! unsafe impl convute::marker::Transmute<u8> for Lt9 {}
//!
//! // Let convute know some u8 are valid Lt9.
//! unsafe impl convute::marker::TryTransmute<Lt9> for u8 {
//!     #[inline]
//!     fn can_transmute(&self) -> bool {
//!         Lt9::new(*self).is_some()
//!     }
//! }
//!
//! fn main() {
//!     let numbers = [1, 6, 10, 5];
//!     let all_lt9: Result<[Lt9; 4], [u8; 4]> = numbers.try_transmute_each();
//!     // Because of the 10, the conversion will fail.
//!     assert!(all_lt9.is_err());
//! }
//! ```
//!
//! Check out the tests in convert.rs for more examples.

mod cast;
pub mod convert;
pub mod marker;
