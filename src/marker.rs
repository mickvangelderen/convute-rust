/// By implementing this trait you promise that every Self is a valid T.
pub unsafe trait Transmute<T: Sized>: Sized {}

/// By implementing this trait you promise that every Self for which
/// self.can_transmute() returns true is a valid T.
pub unsafe trait TryTransmute<T: Sized>: Sized {
    fn can_transmute(&self) -> bool;
}

// All T are valid T.
unsafe impl<T: Sized> Transmute<T> for T {}

// All T are valid T.
unsafe impl<T: Sized> TryTransmute<T> for T {
    #[inline]
    fn can_transmute(&self) -> bool {
        true
    }
}

macro_rules! unsafe_impl_transmute {
    ($A:ty => $B:ty, $($rest:tt)*) => {
        unsafe impl Transmute<$B> for $A {}
        unsafe_impl_transmute!($($rest)*);
    };
    ($A:ty => $B:ty where $can_transmute:expr, $($rest:tt)*) => {
        unsafe impl TryTransmute<$B> for $A {
            #[inline]
            fn can_transmute(&self) -> bool {
                $can_transmute(self)
            }
        }
        unsafe_impl_transmute!($($rest)*);
    };
    () => {}
}

use std::num::*;

unsafe_impl_transmute! {
    // 8
    u8 => i8 where |&val| val < (std::i8::MAX as u8),
    i8 => u8 where |&val| val >= 0,
    NonZeroU8 => u8,
    u8 => Option<NonZeroU8>,
    NonZeroU8 => Option<NonZeroU8>,
    Option<NonZeroU8> => NonZeroU8 where Option::is_some,
    // 16
    u16 => i16 where |&val| val < (std::i16::MAX as u16),
    i16 => u16 where |&val| val >= 0,
    NonZeroU16 => u16,
    u16 => Option<NonZeroU16>,
    NonZeroU16 => Option<NonZeroU16>,
    Option<NonZeroU16> => NonZeroU16 where Option::is_some,
    // 32
    u32 => i32 where |&val| val < (std::i32::MAX as u32),
    i32 => u32 where |&val| val >= 0,
    NonZeroU32 => u32,
    u32 => Option<NonZeroU32>,
    NonZeroU32 => Option<NonZeroU32>,
    Option<NonZeroU32> => NonZeroU32 where Option::is_some,
    // 64
    u64 => i64 where |&val| val < (std::i64::MAX as u64),
    i64 => u64 where |&val| val >= 0,
    NonZeroU64 => u64,
    u64 => Option<NonZeroU64>,
    NonZeroU64 => Option<NonZeroU64>,
    Option<NonZeroU64> => NonZeroU64 where Option::is_some,
    // 128
    u128 => i128 where |&val| val < (std::i128::MAX as u128),
    i128 => u128 where |&val| val >= 0,
    NonZeroU128 => u128,
    u128 => Option<NonZeroU128>,
    NonZeroU128 => Option<NonZeroU128>,
    Option<NonZeroU128> => NonZeroU128 where Option::is_some,
    // size
    usize => isize where |&val| val < (std::isize::MAX as usize),
    isize => usize where |&val| val >= 0,
    NonZeroUsize => usize,
    usize => Option<NonZeroUsize>,
    NonZeroUsize => Option<NonZeroUsize>,
    Option<NonZeroUsize> => NonZeroUsize where Option::is_some,
}
