# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
[Unreleased]: https://gitlab.com/mickvangelderen/convute-rust/compare/0.2.0...master

## [0.2.0] - 2019-03-12
[0.2.0]: https://gitlab.com/mickvangelderen/convute-rust/compare/0.1.1...0.2.0

### Added

- **BREAKING** Automatic implementations `Transmute<T> for T` and
  `TryTransmute<T> for T`. This addition is likely to break type inference!

## [0.1.1] - 2019-03-12
[0.1.1]: https://gitlab.com/mickvangelderen/convute-rust/compare/0.1.0...0.1.1

### Added

- `Transmute` and `TryTransmute` implementations for basic numeric types.

## 0.1.0 - 2019-03-12

Initial release.
