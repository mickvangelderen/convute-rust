[![crates.io](https://img.shields.io/crates/d/convute.svg)](https://crates.io/crates/convute)
[![docs.rs](https://docs.rs/convute/badge.svg)](https://docs.rs/convute)

## Goal

Allow conversion between types that have overlapping valid values. 

## Method

We achieve this through the unsafe marker traits `convute::marker::Transmute`
and `convute::marker::TryTransmute` which enable conversions between values,
references, arrays, vecs and slices.
